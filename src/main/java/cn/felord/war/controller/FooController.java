package cn.felord.war.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author dax
 * @since 2019/11/4 21:45
 */
@RestController
@RequestMapping("/foo")
public class FooController {


    @GetMapping("/bar")
    public String war() {
        return "spring boot war";
    }

}
