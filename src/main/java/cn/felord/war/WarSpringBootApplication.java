package cn.felord.war;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Felordcn
 */
@SpringBootApplication
public class WarSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(WarSpringBootApplication.class, args);
    }

}
